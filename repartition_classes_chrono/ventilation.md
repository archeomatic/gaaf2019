# Script pour **ventiler**

Script R permettant, à partir du tableau de saisie des types de tombes de la région Centre et de leur attribution chronologique, de ventiler celles-ci dans des intervalles de temps (100 ans par défaut) et d'obtenir un graphique représentant des densités de probabilité des tombes par intervalles de temps.

