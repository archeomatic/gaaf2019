 /*  */

select ope.*, typ."nom_type" /* champs à afficher  table."champ" */
from operations as ope /* table principale sur laquelle appliquer la requête */
join type_sep as typ /* jointure1: table fille à joindre */
on ope.id_op = typ.code_sig /* jointure2: champs de jointure entre les 2 tables */
where  lower(typ."nom_type") LIKE '%coffrage%' /* conditions */

 /*  */
 
select   o.*, t."mobilier", count(o.id_op) as nb_site
from operations as o
join type_sep as t
on o."id_op" = t."code_sig"
where lower(t."mobilier") like 'aucun'
group by o."id_op"


 /* sélectionner les sites (Operations) qui contiennent des sépultures (inventaire) du néolithique (-550/-2200) */
 
select ope.*, 'néolithique' as "période", inv.date_debut, inv.date_fin
from Operations as ope join inventaire as inv on ope.id_op = inv.id_op
where inv.date_debut <= -2200 and inv.date_fin > -5000

 /* idem avec regroupement par site et somme des effectifs de sépultures */

select  ope.* /* tous les champs de la table operations */
, 'néolithique' as "période" /* écrire néolithique dans toutes les cases d'un nouveau champ "période */
, min(inv.date_debut) as min_debut, max(inv.date_fin) as max_fin /* pour vérification visuelle, récupération de la plus ancienne date_debut et de la plus récente date_fin */
, sum(inv.effectif_sep) as nb_sep /* somme des effectifs de sépultures */
from Operations as ope join inventaire as inv on ope.id_op = inv.id_op /* jointure de 1 site (operations) à n sépukltures (inventaire) */
where inv.date_debut <= -2200 and inv.date_fin > -5500 /* date_debut <= à la fin de la période ET date_fin >= au début de la période */
group by ope.id_op /* regroupement par opération */

 /* on utilise pour les périodes : */
 /* periode; date_debut; date_fin */
 /* néolithique; -5500; -2200 */
 /* protohistoire; --2200; -50 */
 /* antiquité; -50; 476 */
 /* moyen-âge; 476; -1492 */