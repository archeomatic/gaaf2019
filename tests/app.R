#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#


library(shiny)
# page ui
ui <- fluidPage(
  actionButton("do", "Charger", icon("paper-plane"),
               style="color: #fff; background-color: #337ab7; border-color: #2e6da4")
)

# library
library(gsheet)
library(RPostgreSQL)

# page server
server <- function(input, output, session) {
  observeEvent(input$do, {
    # import des données
    gaaf <- 'https://docs.google.com/spreadsheets/d/1xD-a0YJ7rchGeFN3C0QiTobRmpC5NuYdBSXUopgcWIc/edit#gid=0'
    gaaf <- gsheet2tbl(gaaf)
    # connexion a PostGis
    drv <- dbDriver("PostgreSQL") # on précise le driver
    con <- dbConnect(drv, dbname="archeofrancois_gaaf2019_cvl", host = "postgresql-archeofrancois.alwaysdata.net", user = "archeofrancois_user", password = "chronotypo2019")
    # vérification de la presence de la table et écriture dans postGis
    if(dbExistsTable(con, "inventaire")){ 
      dbRemoveTable(con, "inventaire")}
      dbWriteTable(con, "inventaire", gaaf)
    # deconnexion
    dbDisconnect(con)
    dbUnloadDriver(drv)
    # message d'execution
    print(2+2)
  })
}

shinyApp(ui, server)

