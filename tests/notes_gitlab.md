# Notes sur l'utilisation de gitlab

## Visualisation/Télechargement/Modification d'un fichier

Pour accéder à un fichier il suffit de cliquer dessus.
On peut alors télecharger un fichier en cliquant sur l'icône ![](tests/images/16x16.png "télécharger")

## Accéder à un fichier **.csv** entreposé dans gitlab depuis R

Pour lire un fichier csv déposé dans gitlab, il suffit d'utiliser la fonction read.csv sous cette forme:
```
matable <- read.csv2(url("https://gitlab.com/archeomatic/gaaf2019/raw/master/tests/datesep5.csv"))
```
Pour obtenir le lien web, il faut accéder au fichier puis cliquer sur l'icône Open Raw ![Open Raw](tests/images/open_raw.png "Open Raw") pour afficher les csv.    
Copier/Coller l'adresse web. 

## Supprimer un dossier

Il suffit de supprimer tout son contenu, y compris le fichier .gitkeep