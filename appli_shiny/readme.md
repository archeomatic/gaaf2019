# Application Shiny (version *work in progress*)



application web a été dévellopée pour l'étude, la  rédaction et la publication de la synthèse régionale effectuée dans le  cadre de la [11ème rencontre du GAAF](https://www.gaaf-asso.fr/rencontres/typochronologie-tombe-inhumation/)    et intitulée:              
 **Nouveaux éléments pour la chrono-typologie des  tombes à inhumation en région Centre-Val de Loire et dans le département du Maine-et-Loire, de la Préhistoire à nos jours.**  Livet Jérôme, Gaultier Mathieu, Salé Philippe, Badey Sylvain        





##  ATTENTION: Il existe 2 versions:

- une qui correspond à celle mis en ligne à l'adresse https://archeomatic.shinyapps.io/gaaf2019/ dans le dossier [fonctionelle]

* une version *work in progress*  qui *bug* au déploiement sur shinyapps.io dans le dossier [work_in_progress]



## Faire fonctionner ces applications en local :

* ouvrir les 2 scripts **server.R** et **ui.R ** dans Rstudio
*  cliquer sur l'icône Run App
  

*application dévellopée avec le package R - [Shiny](https://shiny.rstudio.com/)  et hebergée sur la plateforme  [shinyapps.io](https://www.shinyapps.io/)*                              