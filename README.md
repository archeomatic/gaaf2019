# gaaf2019

Ce dépôt a pour objectif d'entrposer des scripts R à partager pour la [11ème rencontre du GAAF](https://www.gaaf-asso.fr/rencontres/typochronologie-tombe-inhumation/) 
qui aura lieu à Tours du 3 au 5 juin 2019 avec pour thème **typochronologie des tombes à inhumations**

Les scripts sont répartis dans des dossiers:

## [Gsheet-R-PostGis](https://gitlab.com/archeomatic/gaaf2019/tree/master/Gsheet-R-PostGis)

Ce dossier contient un script R qui permet de lire le tableau d'inventaire des tombes depuis une table (Google Sheet) est de l'écrire dans une base de données PostGis.    
En automatisant l'éxecution du script cela permet d'exploiter les données saisies dans l'inventaire en ligne dans un projet QGIS. 

## [contingence](https://gitlab.com/archeomatic/gaaf2019/tree/master/contingence)

Ce dossier contient un script R qui permet, à partir de l'inventaire des tombes sur Gsheet, de construire un **tableau de contingence** et de le représenter sous la forme d'un **balloon plot**.    

## [Gantt](https://gitlab.com/archeomatic/gaaf2019/tree/master/gantt) 

Ce dossier contient un script R qui permet de faire un **diagramme de Gantt** pour visualiser l'ordonancement des types de tombes dans le temps.

## [Repartition_classes_chrono](https://gitlab.com/archeomatic/gaaf2019/tree/master/repartition_classes_chrono)

Ce dossier contient un script R qui permet de faire un tableau pour répartir les types de tombes selon un pas chronologique défini puis d'en faire un graphique (barplot).
Un tableau d'inventaire d'exemple est fourni.

## [tests](https://gitlab.com/archeomatic/gaaf2019/tree/master/tests)

Ce dossier contient divers fichiers servant de tests et les notes sur l'utilisation de gitlab.


## Notes:

Chaque dossier comprend *a minima* un script R que vous pouvez visualiser en cliquant dessus puis télecharger en cliquant sur l'icône ![](tests/images/16x16.png "télécharger").    
Parfois il est accompagné d'un fichier texte d'explication: cliquer sur le fichier avec l'extension **.md**. 



